import GetLocalIP
import socket
import SocketServer
import struct
import random
import packet_pb2
#import sys
from Logger import Log

def ip2int(addr):
	ret = struct.unpack("<L",socket.inet_aton(addr))[0]
	return ret

def int2ip(addr):
	ret = socket.inet_ntoa(struct.pack("<L",addr))
	return ret


playerData = dict()
#b = dict()

class XNADDR:
	port = 0
	addr = "0.0.0.0"
	abenet = ""
	abonline = ""

class MyUDPHandler(SocketServer.BaseRequestHandler):

	HANDLERS = {
		packet_pb2.Packet.local_request: "lrequest",
		packet_pb2.Packet.local_confirm: "lconfirm",
		packet_pb2.Packet.secure_request: "srequest",
		packet_pb2.Packet.xnaddr_request: "xrequest"
	}

	def handle(self):
		s = self.request[1]
		data = self.request[0]
		packet = packet_pb2.Packet()
		packet.ParseFromString(data)

		hndlr = self.HANDLERS[packet.type]
		message = getattr(packet,hndlr,None)
		handler = getattr(self,'handle_{0}'.format(hndlr))

		return handler(message)

	def handle_lrequest(self,msg):
		s = self.request[1]
		secure = "0.{}.{}.0".format(random.randint(0,255),random.randint(0,255))
		xnaddr = self.client_address[0]
		xnport = self.client_address[1]
		packet = packet_pb2.Packet()
		packet.type = packet.local_confirm
		packet.lconfirm.secure_addr = ip2int(secure)
		packet.lconfirm.xnaddr = ip2int(xnaddr)
		packet.lconfirm.port = int(xnport)
		packet.lconfirm.abEnet = ''.join(chr(random.randint(0,255)) for _ in range(6))
		packet.lconfirm.abOnline = ''.join(chr(random.randint(0,255)) for _ in range(20))

		Log( "Local request response to: {}:{}".format(self.client_address[0],self.client_address[1]) )
		Log( "abEnet: "+packet.lconfirm.abEnet.encode('hex') )
		Log( "abOnline: "+packet.lconfirm.abOnline.encode('hex') )
		Log( "xnaddr: %08X" % (packet.lconfirm.xnaddr) )
		Log( "port: %08X" % (xnport) )
		Log( "secure: %08X" % ip2int(secure) )

		s.sendto(packet.SerializeToString(),(self.client_address[0],self.client_address[1]))

	def handle_lconfirm(self,msg):
		s = self.request[1]

		secure = msg.secure_addr
		xnaddr = msg.xnaddr
		port = msg.port
		abOnline = msg.abOnline
		abEnet = msg.abEnet

		xn = XNADDR()
		xn.port = port
		xn.addr = xnaddr
		xn.abonline = abOnline
		xn.abenet = abEnet
		Log( "- lconfirm - from: {}:{}".format(self.client_address[0],self.client_address[1]) )
		Log( "abOnline: " + abOnline.encode('hex') )
		Log( "abEnet: " + abEnet.encode('hex') )
		Log( "secure: %08X" % (secure) )
		Log( "xnaddr: %08X" % (xnaddr) )
		Log( "port: %08X" % (port) )

		playerData[int2ip(secure)] = xn
		playerData[abEnet.encode("hex")] = secure



	def handle_xrequest(self,msg):
		s = self.request[1]

		Log( "-xrequest- request" )
		Log( "request from: {}:{}".format(self.client_address[0],self.client_address[1]) )
		Log( "secure: %08X" % (msg.secure) )

		packet = packet_pb2.Packet()
		packet.type = packet.xnaddr_reply
		xn = XNADDR()

		if int2ip(msg.secure) in playerData:
			Log( "MethodA" )
			xn = playerData[int2ip(msg.secure)]
		elif msg.secure in playerData:
			Log( "MethodB" )#likely will never work, worth a try...
			xn = playerData[msg.secure]
		else:
			Log( "playerData does not contain {} or {}".format(msg.secure,int2ip(msg.secure)) )
			xn.addr = ip2int(xn.addr)

		packet.xreply.xnaddr = xn.addr
		packet.xreply.port = xn.port
		packet.xreply.abEnet = xn.abenet
		packet.xreply.abOnline = xn.abonline

		Log( "-xrequest- response to: {}:{} ".format(self.client_address[0],self.client_address[1]) )
		Log( "responding to: " + self.client_address[0] )
		Log( "abEnet: " + xn.abenet.encode('hex') )
		Log( "abOnline: " + xn.abonline.encode('hex') )
		Log( "port: %08X" % (xn.port) )
		Log( "xnaddr: %08X" % (xn.addr) )
		Log( "secure: %08X" % (msg.secure) )

		s.sendto(packet.SerializeToString(),(self.client_address[0],self.client_address[1]))


	def handle_srequest(self,msg):
		s = self.request[1]
		Log( "-srequest from: {}:{} ".format(self.client_address[0],self.client_address[1]) )
		
		secure = ip2int("0.0.0.0")
		if msg.abEnet.encode("hex") in playerData:
			secure = playerData[msg.abEnet.encode("hex")]
		else:
			Log( "abEnet not in playerData: {}".format(msg.abEnet.encode("hex")) )

		packet = packet_pb2.Packet()
		packet.type = packet.secure_reply
		packet.sreply.secure = secure
		Log( "abEnet: " + msg.abEnet.encode('hex') )
		Log( "secure: %08X" % (secure) )

		s.sendto(packet.SerializeToString(),(self.client_address[0],self.client_address[1]))

if __name__ == "__main__":
	BindIP = GetLocalIP.getLocalIP()
	#BindIP = "10.0.0.17"
	BindPort = 27019
	Log( "Binding to (Local) IP on Port: {}:{}".format(BindIP, BindPort) )
	HOST, PORT = BindIP, BindPort
	server = SocketServer.UDPServer((HOST,PORT), MyUDPHandler)
	server.serve_forever()


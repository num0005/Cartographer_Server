import GetLocalIP
import SocketServer
import datetime
from Logger import Log

dedicatedGameServers = dict()

# Preset Dedicated Servers

dedicatedGameServers[("73.204.13.78",1001)] = (datetime.datetime.now(),0)#Server FL - KitKat
dedicatedGameServers[("107.5.22.241",1001)] = (datetime.datetime.now(),0) #Rude Yoshi
dedicatedGameServers[("52.40.131.246",1001)] = (datetime.datetime.now(),0) #Rude Yoshi 2
#dedicatedGameServers[("52.87.236.8",1001)] = (datetime.datetime.now(),0)

clients = dedicatedGameServers.copy()

# Random users to restore on restart
clients[("24.251.198.62",1001)] = (datetime.datetime.now(),0)

class MyUDPHandler(SocketServer.BaseRequestHandler):

	def handle(self):
		s = self.request[1]
		data = self.request[0]

		broadcast_search_type = 0 # The Game Server replies to indicate to all users that a lobby is available
		if (data[0].encode('hex') == "07"):
			Log( "{}:{} - wrote broadcast-reply.".format(self.client_address[0],self.client_address[1]) )
		if (data[0].encode('hex') == "05"):
			broadcast_search_type = 1 # The Client requests a search to be run to find available lobbys
		
		player = self.client_address
		annoyedLast = datetime.datetime.now()
		annoyanceFactor = 0

		if (clients.get(player, None) != None and broadcast_search_type == 1):
			annoyedLast = clients[player][0]
			annoyanceFactor = clients[player][1]
			if (datetime.datetime.now() - annoyedLast < datetime.timedelta(seconds=4)):
				annoyanceFactor += 1
			else:
				annoyanceFactor = 0
		
		clients[player] = (datetime.datetime.now(), annoyanceFactor)
		
		# Roughly 45 seconds of idle time in search menu.
		if (annoyanceFactor > 45):
			Log( "Ignoring requesting player ({}) at {}:{}".format(annoyanceFactor, player[0],player[1]) )
		else:
			# Now since it hasn't been ignored, log the action.
			if (broadcast_search_type == 1):
				Log( "{}:{} ({}) - wrote broadcast-search.".format(player[0],player[1], annoyanceFactor) )
			
			sent_to_string = "";
			
			for dest in clients.keys():
				# Remove any invalid entries (if they ever occur)
				if ( clients.get(dest, None) == None or type(clients.get(dest, None)) != type(()) or len(clients.get(dest, None)) != 2 ):
					Log("There was an invalid entry {}".format(dest))
					if (clients.get(dest, None) != None):
						Log("Invalid's Data: {}".format(clients.get(dest, None)))
					del clients[dest]
					continue
				
				# Sends out search for hosts
				if (broadcast_search_type == 1):
					sent_to_string += "{}:{}, ".format(dest[0],dest[1])
					s.sendto(data, (dest[0],dest[1]) )
				# Sends replies to active listeners.
				elif (dest not in dedicatedGameServers and broadcast_search_type == 0 and datetime.datetime.now() - clients.get(dest, None)[0] < datetime.timedelta(seconds=5) ):
					sent_to_string += "{}:{}, ".format(dest[0],dest[1])
					s.sendto(data, (dest[0],dest[1]) )
				
				# Gets rid of those excess IP's that cant host and have idled.
				if (dest[1] != 1001 and datetime.datetime.now() - clients.get(dest, 0)[0] > datetime.timedelta(minutes=1)):
					Log( "Removing >1 minute old non-host entry: {}:{}".format(dest[0],dest[1]) )
					del clients[dest]
					continue
				
				if (dest not in dedicatedGameServers and broadcast_search_type == 1):
					# Removes inactive players from search list.
					if (datetime.datetime.now() - clients.get(dest, 0)[0] > datetime.timedelta(minutes=20)):
						Log( "Removing >20 minute old entry: {}:{}".format(dest[0],dest[1]) )
						del clients[dest]
						continue
			
			# Logs sent packets to terminal.
			if (broadcast_search_type == 1):
				Log( "Sending broadcast-search to: {}".format(sent_to_string) )
			elif (broadcast_search_type == 0):
				Log( "Sending broadcast-reply to: {}".format(sent_to_string) )

if __name__ == "__main__":
	BindIP = GetLocalIP.getLocalIP()
	#BindIP = "10.0.0.17"
	BindPort = 1001
	Log( "Binding to (Local) IP on Port: {}:{}".format(BindIP, BindPort) )
	HOST, PORT = BindIP, BindPort
	server = SocketServer.UDPServer((HOST,PORT), MyUDPHandler)
	server.serve_forever()


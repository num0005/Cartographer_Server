from datetime import datetime

def Log (text):
	print "{} > {}".format(datetime.strftime(datetime.now(), '%H:%M:%S').lower(), text)